#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "main.h"

xTaskCreate( vTask1 , "Test", configMINIMAL_STACK_SIZE, NULL, mainTestTask_Priority, NULL );
vTaskStartScheduler();  
static void Task3( void *pvParameters );
void vTaskDelay( portTickType xTicksToDelay );

SemaphoreHandle_t xSemaphore;
ySemaphore = xSemaphoreCreateBinary();  	
xSemaphore = xSemaphoreCreateMutex();


if( xSemaphoreTake( xSemaphore, 3 ) == pdTRUE ) 
if( xSemaphoreTake( xSemaphore, portMAX_DELAY ) == pdTRUE )  
     
xSemaphoreGive( xSemaphore );
taskYIELD();

const portTickType xDelay = 1000 / portTICK_RATE_MS;
vTaskDelay (xDelay);

//______________________________________________
void EXTI15_10_IRQHandler(void)
{
long lHigherPriorityTaskWoken = pdFALSE;

EXTI_ClearITPendingBit( EXTI_Line13 );

xSemaphoreGiveFromISR( x_IRQ_Semaphore, &lHigherPriorityTaskWoken );
portEND_SWITCHING_ISR( lHigherPriorityTaskWoken );
}

//_________________________________________________________

void USART2_IRQHandler(void)
{
	if (USART_GetITStatus(USART2, USART_IT_RXNE))
	{
		long lHigherPriorityTaskWoken = pdFALSE;
		xSemaphoreGiveFromISR( USART_Sem, &lHigherPriorityTaskWoken );
		USART_ClearITPendingBit(USART2, USART_IT_RXNE);
  }
}
//______________________________________________

#include "croutine.h"
vCoRoutineSchedule();
xCoRoutineCreate( vFlashCoRoutine, 0, uxIndex );
void vFlashCoRoutine( CoRoutineHandle_t xHandle, UBaseType_t uxIndex )
 {
     crSTART( xHandle );
     for( ;; )
     {
        LEDGreenon();
         crDELAY( xHandle, 200 );
			  LEDGreenoff();
         crDELAY( xHandle, 200 );
     }
     crEND();
 }
//______________________________________________

QueueHandle_t myQu;
 myQu = xQueueCreate( 10, sizeof(char) );

xQueueSend( myQu, &myChar, (portTickType) 1 );
if (xQueueReceive( myQu, &myChar, (portTickType) 10 )) {
		}

if (USART_GetFlagStatus(USART2, USART_FLAG_RXNE)) {
			myChar=USART_ReceiveData(USART2);
		  xQueueSend( myQu, &myChar, (portTickType) 1 );
		}

if (xQueueReceive( USARTQ, &sendChar, (portTickType) 10 )) {
			USART_SendData(USART2, sendChar);
			}
		vTaskDelay(1);

//_________________________________________________________

sprintf(sendString, "%s","LED 1 ");
for (int j=0; sendString[j] != '\0'; j++) {
xQueueSend( USARTQ, &sendString[j], (portTickType) 10 );	
					}

for(int i = 0; startText[i] != '\0'; i++){
		USART_SendData(USART2, startText[i]);
		vTaskDelay (1);
	}

if( xSemaphoreTake( USART_Sem, portMAX_DELAY ) == pdTRUE ){
				Zeichen = USART_ReceiveData(USART2);
}
